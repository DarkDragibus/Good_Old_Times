# ----------------------------------------------------------------------------
# "THE BEER-WARE LICENSE" (Revision 42.1):
# DarkDragibus wrote this file. This software wasn't made to harm anyone in any way.
# As long as you retain this notice you can do whatever you want with 
# this stuff. If we meet some day, and you think this stuff is worth it,
# you owe me a beer in return
# ----------------------------------------------------------------------------
from bs4 import BeautifulSoup
import re

class Employee:

    #Personnal infos
    name = ""
    firstName = ""
    birthDay = ""

    #career infos
    careerSport   = ""
    careerSchool  = ""
    careerCompany = ""

    #Current infos
    currentCity = ""
    currentCityCountry = ""
    currentJob = ""
    currentFamily = ""

    #Interests infos
    hobbies = ""
    instruments = ""
    musics = ""
    sports = ""
    readings = ""
    cars = ""
    pets = ""

    #Countries infos
    countriesVisited = ""
    countriesWanted = ""

    ## -----------------------------------------------
    ## Constructor
    def __init__(self, webPage):
        soup = BeautifulSoup(webPage,"lxml")
        self.extractDataFromPage(soup)

    ## -----------------------------------------------
    ## Webpage to data cloud
    def extractDataFromPage(self,soup):
        self.extractPersonnalsInfo(soup)
        self.extractCareersInfo(soup)
        self.extractCurrentInfo(soup)
        self.extractInterestsInfo(soup)
        self.extractCountriesInfo(soup)

    ## -----------------------------------------------
    ## Extract personnal infos
    def extractPersonnalsInfo(self,soup):
        ## ============
        tmp = soup.find('span', class_="fn")
        if(tmp != None ):
            tmp = tmp.text.split()
            self.firstName = tmp[0]
            self.name = tmp[1]

        tmp = soup.find('abbr', class_="bday")
        if(tmp != None ):
            self.birthDay = tmp.text

    ## -----------------------------------------------
    ## Extract club/school/carreer infos
    def extractCareersInfo(self,soup):
        ## ============
        sections = soup.find_all('section')
        for section in sections:
            if("Parcours" in section.text):
                section = section.text.split('Parcours')
                for career in section :
                    career = career.split()
                    if(career == []):
                        #do nothing
                        continue
                    elif(career[0] == "club"):
                        self.careerSport = ' '.join(career[1:])
                    elif(career[0] == "scolaire"):
                        self.careerSchool = ' '.join(career[1:])
                    elif(career[0] == "entreprise"):
                        self.careerCompany = ' '.join(career[1:])
        return


    ## -----------------------------------------------
    ## Extract current situation information
    def extractCurrentInfo(self,soup):
        city = soup.find('span', class_="locality")
        country = soup.find('span', class_="country-name")
        job = soup.find('span', class_="locality")

        if(city != None): #if parsing worked
            self.currentCity = city.text

        if(country != None): #if parsing worked
            self.currentCityCountry = country.text

        if(job != None): #if parsing worked
            self.currentJob = job.text


        #Getting familial situation is a little bit more tricky
        situation = soup.find(class_="jMyLifemyLife")

        #check if situation exists
        if(situation == None):
            return

        #Get whole situation
        situation = situation.text

        #Split along \n
        situation = re.sub('\n+','\n',situation).split('\n')

        #get fifth arg
        if(len(situation) > 4):
            self.currentFamily = situation[4]

        return

    ## -----------------------------------------------
    ## Extract current situation information
    def extractInterestsInfo(self,soup):
        interests = soup.find(class_="jPassions")

        if(interests == None):
            return

        interests = interests.text.split("            ")
        for i in range(0,len(interests)):
            #remove all \n
            interests[i] = re.sub('\n+',' ',interests[i])
            #replace group of spaces by only one spaces
            interests[i] = re.sub(' +',' ',interests[i])

        while(' ' in interests):
            interests.remove(' ')

        for i in range(0,len(interests)-1):
            if("Loisirs" in interests[i]):
                self.hobbies = interests[i+1]

            if("usique" in interests[i]):
                self.instruments = interests[i+1]
            
            elif("musicaux" in interests[i]):
                self.musics = interests[i+1]
            
            elif("Sports" in interests[i]):
                self.sports = interests[i+1]
            
            elif("Lectures" in interests[i]):
                self.readings = interests[i+1]
            
            elif("Voitures" in interests[i]):
                self.cars = interests[i+1]
            
            elif("Animaux" in interests[i]):
                self.pets = interests[i+1]


    ## -----------------------------------------------
    ## Extract current situation information
    def extractCountriesInfo(self,soup):
        listOfCountries = soup.find(class_="jAllreadyBeen")

        if(listOfCountries == ""):
            self.countriesVisited = listOfCountries.text.split('\n')[3]

        listOfCountries = soup.find(class_="jWishBeen")
        if(listOfCountries == ""):
            self.countriesVisited = listOfCountries.text.split('\n')[3]


    ## -----------------------------------------------
    ## Extract current situation information
    def toCsvString(self):
        output = ""
        #Personnal infos
        output+=self.name + ";"
        output+=self.firstName + ";"
        output+=self.birthDay + ";"
        output+=self.careerSport + ";"
        output+=self.careerSchool + ";"
        output+=self.careerCompany + ";"
        output+=self.currentCity + ";"
        output+=self.currentCityCountry + ";"
        output+=self.currentJob + ";"
        output+=self.currentFamily + ";"
        output+=self.hobbies + ";"
        output+=self.instruments + ";"
        output+=self.musics + ";"
        output+=self.sports + ";"
        output+=self.readings + ";"
        output+=self.cars + ";"
        output+=self.pets + ";"
        output+=self.countriesVisited + ";"
        output+=self.countriesWanted + ";"
        return output