#!/usr/bin/python3

# ----------------------------------------------------------------------------
# "THE BEER-WARE LICENSE" (Revision 42.1):
# DarkDragibus wrote this file. This software wasn't made to harm anyone in any way.
# As long as you retain this notice you can do whatever you want with 
# this stuff. If we meet some day, and you think this stuff is worth it,
# you owe me a beer in return
# ----------------------------------------------------------------------------

from bs4 import BeautifulSoup
import urllib3
import sys
import employee

## -----------------------------------------------
def main(argv):
    printheader()
    if(len(argv) < 3):
        print("usage : ./Good_Old_Times.py [URL] [filename.csv]")
    else:
        getEmployeesInfo(argv[1], argv[2])
        # export results as csv

## -----------------------------------------------
def getEmployeesInfo(companyUrl, filename):    
    companyEmployeesURLlist = extractEmployeesFromCompanyPage(companyUrl)

    # No employees have been found
    if(len(companyEmployeesURLlist) == 0):
        return;

    employeeList = extractEmployeesInfo(companyEmployeesURLlist)
    exportEmployeesInfo(employeeList, filename)

## -----------------------------------------------
def extractEmployeesFromCompanyPage(companyUrl):
    urlList = []
    extractUrlFromPage(companyUrl, urlList, 1)
    print("\n%d employees found !" % len(urlList))
    return urlList

## -----------------------------------------------
def extractUrlFromPage(companyUrl, employeesUrlList, pageNumber):
    #----------------------------
    #Simple HMI
    print("Parsing %s  ..." % companyUrl, end='\r', flush=True)

    #----------------------------
    #Parse page
    companyPage = getPage(companyUrl)
    soup = BeautifulSoup(companyPage,"lxml")
    employeesHtmlList = soup.find_all('a', class_="jTinyProfileUser notip")
  
    #----------------------------
    #Parsed page exists ?
    if(employeesHtmlList == []):
        return(False)
    else:
        #----------------------------
        #Scroll page 
        for employee in employeesHtmlList :
            employeesUrlList.append(\
                "http://copainsdavant.linternaute.com" + employee['href'])

        #----------------------------
        #check if a next page exists!
        if(pageNumber == 1 and "?page=" not in companyUrl):
            #create "next page" field
            companyUrl += "?page="

        #remove previous number xx in ?page=xx
        while(companyUrl[-1] != '='):
            companyUrl = companyUrl[:-1]

        companyUrl += str(pageNumber+1)

        #----------------------------
        #recursive call on next page
        extractUrlFromPage(companyUrl, employeesUrlList, pageNumber+1)

    #----------------------------
    #End of function
    return(True)



## -----------------------------------------------
def extractEmployeesInfo(companyEmployeesURLlist):
# ease developpement, calling just once exportDataCsv

    employeeList = []
    i = 0
    maxUrls = len(companyEmployeesURLlist)

    for employeesURL in companyEmployeesURLlist:
        print("Parsing employees ... %.2f%%" % ((100*i)/maxUrls), end='\r', flush=True)

        i+=1
        employeePage = getPage(employeesURL)
        tmp = employee.Employee(employeePage)
        employeeList.append(tmp)

    print("Done ! (%d stalks succesful/ %d found)" % (len(employeeList),maxUrls))

    return(employeeList)
## -----------------------------------------------
def exportEmployeesInfo(list,filename):
    f = open(filename,"w")
    f.write( "Nom;Prenom;Date de naissance;\
        Carriere sportive;Parcours scolaire;Carriere;\
        Ville actuelle;Pays;Travail;Situation;Loisirs;\
        Instruments de Musique;Gouts musicaux;Sports pratiques;\
        Lectures;voiture;animaux;A visite;aimerais aller")

    for employee in list:
        f.write("\n" + employee.toCsvString())

    f.close()

## -----------------------------------------------
def getPage(companyUrl):
    http_pool = urllib3.connection_from_url(companyUrl)
    r = http_pool.urlopen('GET',companyUrl)
    return (r.data.decode('utf-8','ignore'))


## -----------------------------------------------
def printheader():
    print("""
      ________                  .___   _______  .__       .___  _________.__                       
     /  _____/  ____   ____   __| _/   \   _  \ |  |    __| _/  \__  ___/|__| _____   ____   ______
    /   \  ___ /    \ /    \ / __ |    /  /_\  \|  |   / __ |     |  |   |  |/     \_/ __ \ /  ___/
    \    \_\  (  <0> | <0>  ) /_/ |    \  \_/   \  |__/ /_/ |     |  |   |  |  Y Y  \  ___/ \___ \ 
     \______  /\____/ \____/\____ |     \_____  /____/\____ |     |__|   |__|__|_|  /\___  >____  >
            \/                   \/           \/           \/                     \/     \/     \/ 
                                              v1.1.1
                                         by Dark Dragibus

           """)

## -----------------------------------------------
if __name__ == "__main__":
   main(sys.argv)
